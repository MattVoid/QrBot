import telepot
import os
import sys
from time import sleep
from PIL import Image
import zbarlight
import re
import pyqrcode

step = 0
text = 0

def comand(msg):
    global step, text
    content_type, chat_type, chat_id = telepot.glance(msg) 
    chat_id = msg['chat']['id']

    if content_type == 'text' and msg['text'] == '/start':
        bot.sendMessage(chat_id, 'Questo bot gestisce qr code, usa /encode per creare un qr code, usa invece /decode per decodificarne uno')

    if content_type == 'text' and msg['text'] == '/encode':
        bot.sendMessage(chat_id, 'Scegli parola da codificare in qr')
        step = 1

    if content_type == 'text' and msg['text'] != '/start' and msg['text'] != '/encode' and msg['text'] != '/decode' and step == 1:
        name = str(msg['text'])
        text = name
        code = pyqrcode.create(name)
        code.png(name + ".png", scale = 10)
        bot.sendChatAction(chat_id, 'upload_document')
        bot.sendPhoto(chat_id, open(name + '.png', 'rb'))
        os.system('rm -r ' + name + '.png')
        #https://stackoverflow.com/questions/35314526/telegram-bot-telepot-api-is-it-possible-to-send-an-image-directly-from-url-wi
        #https://github.com/python-telegram-bot/python-telegram-bot/wiki/Code-snippets#download-a-file
        step = 0

    if content_type == 'text' and msg['text'] == '/decode':
        bot.sendMessage(chat_id, 'Manda qr code da decodificare')
        step = 2

    if content_type == 'photo' and step == 2:
        name = msg['photo'][0]['file_id']
        print(str(name))
        photo = msg['photo'][-1]['file_id']
        bot.download_file(photo, './' + name)
        image = Image.open(name)
        image.load()
        codes = zbarlight.scan_codes("qrcode", image)
        bot.sendMessage(chat_id, str(codes).strip("[]b\'"))
        os.system('rm -r ' + name)
        step = 0

TOKEN = '506645813:AAGnrk-7DlGep12n1wxAKEONQkiZ4FwI73s'

# Main
print("Avvio bot")


bot = telepot.Bot(TOKEN)
bot.message_loop(comand)
while(1):
    sleep(10)